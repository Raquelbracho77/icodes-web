import React from 'react';
import ReactDOM from 'react-dom';
import Icodes from './app';

ReactDOM.render(<Icodes />, document.getElementById('root'));